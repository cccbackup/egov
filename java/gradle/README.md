# Gradle

* [維基百科:Gradle](https://zh.wikipedia.org/wiki/Gradle)

## Install

* https://gradle.org/
    * https://gradle.org/install/

```
PS D:\ccc109> gradle -v

Welcome to Gradle 6.7.1!

Here are the highlights of this release:
 - File system watching is ready for production use
 - Declare the version of Java your build requires 
 - Java 15 support

For more details see https://docs.gradle.org/6.7.1/release-notes.html


------------------------------------------------------------
Gradle 6.7.1
------------------------------------------------------------

Build time:   2020-11-16 17:09:24 UTC
Revision:     2972ff02f3210d2ceed2f1ea880f026acfbab5c0

Kotlin:       1.3.72
Groovy:       2.5.12
Ant:          Apache Ant(TM) version 1.10.8 compiled on May 10 2020
JVM:          15.0.1 (Oracle Corporation 15.0.1+9-18)
OS:           Windows 10 10.0 amd64
```

## Use

```sh
PS D:\ccc109\egov\java\00-install\02-gradle> gradle init
Starting a Gradle Daemon (subsequent builds will be faster)

Select type of project to generate:
  1: basic
  2: application
  3: library
  4: Gradle plugin
Enter selection (default: basic) [1..4] 2

Select implementation language:
  1: C++
  2: Groovy
  3: Java
  4: Kotlin
  5: Scala
  6: Swift
Enter selection (default: Java) [1..6] 3

Split functionality across multiple subprojects?:
  1: no - only one application project
  2: yes - application and library projects
Enter selection (default: no - only one application project) [1..2] 1

Select build script DSL:
  1: Groovy
  2: Kotlin
Enter selection (default: Groovy) [1..2] 1

Select test framework:
  1: JUnit 4
  2: TestNG
  3: Spock
  4: JUnit Jupiter
Enter selection (default: JUnit 4) [1..4] 1

Project name (default: 02-gradle): gradleHello
Source package (default: gradleHello):

> Task :init
Get more help with your project: https://docs.gradle.org/6.7.1/samples/sample_building_java_applications.html   

BUILD SUCCESSFUL in 1m 46s
2 actionable tasks: 2 executed
PS D:\ccc109\egov\java\00-install\02-gradle> ./gradlew run
Downloading https://services.gradle.org/distributions/gradle-6.7.1-bin.zip
.........10%..........20%..........30%..........40%..........50%.........60%..........70%..........80%..........90%..........100%

> Task :app:run
Hello World!

BUILD SUCCESSFUL in 1m 57s
2 actionable tasks: 2 executed
PS D:\ccc109\egov\java\00-install\02-gradle> ./gradlew build

BUILD SUCCESSFUL in 16s
7 actionable tasks: 6 executed, 1 up-to-date
PS D:\ccc109\egov\java\00-install\02-gradle> ./gradlew build --scan

BUILD SUCCESSFUL in 16s
7 actionable tasks: 7 up-to-date

Publishing a build scan to scans.gradle.com requires accepting the Gradle Terms of Service defined at https://gradle.com/terms-of-service. Do you accept these terms? [yes, no] y
Please enter 'yes' or 'no': yes

Gradle Terms of Service accepted.

Publishing build scan...
https://gradle.com/s/ettzbgfkvrboe
```


![](./img/GradleScan.png)

## Run

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/gradle (master)
$ ./gradlew run

> Task :app:run
Hello World!

BUILD SUCCESSFUL in 4s
2 actionable tasks: 1 executed, 1 up-to-date
```