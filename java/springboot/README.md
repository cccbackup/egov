# Spring Boot

* https://spring.io/guides/gs/rest-service/
    * https://github.com/spring-guides/gs-rest-service

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/springboot/gs-rest-service/initial (master)        
$ ./gradlew build
<=========----> 75% EXECUTING [1m 0s]

> Task :test
2021-01-03 09:08:57.794  INFO 5364 --- [extShutdownHook] o.s.s.concurrent.ThreadPoolTaskExecutor  : 
Shutting down ExecutorService 'applicationTaskExecutor'

BUILD SUCCESSFUL in 1m 7s
5 actionable tasks: 3 executed, 2 up-to-date

user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/springboot/gs-rest-service/initial (master)
$ java -jar build/libs/gs-rest-service-0.1.0.jar
Error: Unable to access jarfile build/libs/gs-rest-service-0.1.0.jar

user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/java/springboot/gs-rest-service/initial (master)
$ java -jar ./build/libs/rest-service-0.0.1-SNAPSHOT.jar

  .   ____          _            __ _ _ 
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.3.2.RELEASE)

2021-01-03 09:10:04.998  INFO 4120 --- [           main] c.e.restservice.RestServiceApplication   : 
Starting RestServiceApplication on DESKTOP-96FRN6B with PID 4120 (D:\ccc109\egov\java\springboot\gs-rest-service\initial\build\libs\rest-service-0.0.1-SNAPSHOT.jar started by user in d:\ccc109\egov\java\springboot\gs-rest-service\initial)
2021-01-03 09:10:05.020  INFO 4120 --- [           main] c.e.restservice.RestServiceApplication   : 
No active profile set, falling back to default profiles: default
2021-01-03 09:10:10.172  INFO 4120 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : 
Tomcat initialized with port(s): 8080 (http)
2021-01-03 09:10:10.256  INFO 4120 --- [           main] o.apache.catalina.core.StandardService   : 
Starting service [Tomcat]
2021-01-03 09:10:10.394  INFO 4120 --- [           main] org.apache.catalina.core.StandardEngine  : 
Starting Servlet engine: [Apache Tomcat/9.0.37]
2021-01-03 09:10:10.871  INFO 4120 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : 
Initializing Spring embedded WebApplicationContext
2021-01-03 09:10:10.883  INFO 4120 --- [           main] w.s.c.ServletWebServerApplicationContext : 
Root WebApplicationContext: initialization completed in 5133 ms
2021-01-03 09:10:12.289  INFO 4120 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : 
Initializing ExecutorService 'applicationTaskExecutor'
2021-01-03 09:10:13.199  INFO 4120 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : 
Tomcat started on port(s): 8080 (http) with context path ''
2021-01-03 09:10:13.254  INFO 4120 --- [           main] c.e.restservice.RestServiceApplication   : 
Started RestServiceApplication in 11.627 seconds (JVM running for 14.012)
2021-01-03 09:10:35.723  INFO 4120 --- [nio-8080-exec-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : 
Initializing Spring DispatcherServlet 'dispatcherServlet'
2021-01-03 09:10:35.726  INFO 4120 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : 
Initializing Servlet 'dispatcherServlet'
2021-01-03 09:10:35.754  INFO 4120 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : 
Completed initialization in 26 ms
```

http://localhost:8080/greeting

看到

```
Whitelabel Error Page
This application has no explicit mapping for /error, so you are seeing this as a fallback.

Sun Jan 03 09:10:36 CST 2021
There was an unexpected error (type=Not Found, status=404).
```