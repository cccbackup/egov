# X-Road : Playground

* https://x-road.global/xroad-playground

![](./img/playground1.png)


## ACCESSING SECURITY SERVERS
The three Security Servers in the environment have different roles: Central Server's Security Server (1), service consumer's Security Server (2) and service provider's Security Server (3). NIIS is the owner of Security Server #1, Test company is the owner of Security Server #2 and Test agency the owner of Security Server #3. The test services are published on Security Server #3 and they can be consumed via Security Server #2.

Central Server's Security Server:

* https://niisss01.playground.x-road.global:4000

Service consumer's Security Server:

* https://testcomss01.playground.x-road.global:4000

Service provider's Security Server:

* https://testagess01.playground.x-road.global:4000

Username for the UI is "xrd" and password is "secret"


## Central Server's Security Server:

> Username:xrd
> password:secret

https://niisss01.playground.x-road.global:4000

![](D:\ccc109\egov\xroad\playground\img\playgroundCentralSecurityServer.png)

## SOAP service

## getRandom

* https://x-road.global/s/getRandom.xml

browser

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xro="http://x-road.eu/xsd/xroad.xsd" xmlns:iden="http://x-road.eu/xsd/identifiers" >
    <soapenv:Header>
        <xro:client iden:objectType="SUBSYSTEM">
            <iden:xRoadInstance>PLAYGROUND</iden:xRoadInstance>
            <iden:memberClass>COM</iden:memberClass>
            <iden:memberCode>1234567-8</iden:memberCode>
            <iden:subsystemCode>TestClient</iden:subsystemCode>
        </xro:client>
        <xro:service iden:objectType="SERVICE">
            <iden:xRoadInstance>PLAYGROUND</iden:xRoadInstance>
            <iden:memberClass>GOV</iden:memberClass>
            <iden:memberCode>8765432-1</iden:memberCode>
            <iden:subsystemCode>TestService</iden:subsystemCode>
            <iden:serviceCode>getRandom</iden:serviceCode>
            <iden:serviceVersion>v1</iden:serviceVersion>
        </xro:service>	  
        <xro:id>ID11234</xro:id>
        <xro:userId>EE1234567890</xro:userId>
        <xro:protocolVersion>4.0</xro:protocolVersion>
    </soapenv:Header>
    <soapenv:Body>
        <prod:getRandom xmlns:prod="http://test.x-road.fi/producer">
            <prod:request/>
        </prod:getRandom>   
    </soapenv:Body>
</soapenv:Envelope>
```

curl

```
$ curl -d @getRandom.xml --header "Content-Type: text/xml" -X POST http://testcomss01.playground.x-
road.global
Warning: Couldn't read data from file "getRandom.xml", this makes an empty 
Warning: POST.
<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Body><SOAP-ENV:Fault><faultcode>Client.InvalidSoap</faultcode><faultstring>org.xml.sax.SAXParseException; Premature end of file.</faultstring><faultactor></faultactor><detail><faultDetail xmlns="">58063980-d334-4ed3-9de4-4c858cbb5c0e</faultDetail></detail></SOAP-ENV:Fault></SOAP-ENV:Body></SOAP-ENV:Envelope>
```

## HelloService

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xro="http://x-road.eu/xsd/xroad.xsd" xmlns:iden="http://x-road.eu/xsd/identifiers">
    <soapenv:Header>
        <xro:client iden:objectType="SUBSYSTEM">
            <iden:xRoadInstance>PLAYGROUND</iden:xRoadInstance>
            <iden:memberClass>COM</iden:memberClass>
            <iden:memberCode>1234567-8</iden:memberCode>
            <iden:subsystemCode>TestClient</iden:subsystemCode>
        </xro:client>
        <xro:service iden:objectType="SERVICE">
            <iden:xRoadInstance>PLAYGROUND</iden:xRoadInstance>
            <iden:memberClass>GOV</iden:memberClass>
            <iden:memberCode>8765432-1</iden:memberCode>
            <iden:subsystemCode>TestService</iden:subsystemCode>
            <iden:serviceCode>helloService</iden:serviceCode>
            <iden:serviceVersion>v1</iden:serviceVersion>
        </xro:service>
        <xro:id>ID11234</xro:id>
        <xro:userId>EE1234567890</xro:userId>
        <xro:protocolVersion>4.0</xro:protocolVersion>
    </soapenv:Header>
    <soapenv:Body>
        <prod:helloService xmlns:prod="http://test.x-road.global/producer">
            <prod:name>Test</prod:name>
        </prod:helloService>
    </soapenv:Body>
</soapenv:Envelope>
```

curl

```
$ curl -d @helloService.xml --header "Content-Type: text/xml" -X POST http://testcomss01.playground
.x-road.global
Warning: Couldn't read data from file "helloService.xml", this makes an empty 
Warning: POST.
<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Body><SOAP-ENV:Fault><faultcode>Client.InvalidSoap</faultcode><faultstring>org.xml.sax.SAXParseException; Premature end of file.</faultstring><faultactor></faultactor><detail><faultDetail xmlns="">56fc075d-dab5-4b51-8c4d-4adb2c8aed09</faultDetail></detail></SOAP-ENV:Fault></SOAP-ENV:Body></SOAP-ENV:Envelope>
```

## listMethods

browser

```xml
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:iden="http://x-road.eu/xsd/identifiers" xmlns:xrd="http://x-road.eu/xsd/xroad.xsd">
    <SOAP-ENV:Header>
        <xrd:client iden:objectType="SUBSYSTEM">
            <iden:xRoadInstance>PLAYGROUND</iden:xRoadInstance>
            <iden:memberClass>COM</iden:memberClass>
            <iden:memberCode>1234567-8</iden:memberCode>
            <iden:subsystemCode>TestClient</iden:subsystemCode>
        </xrd:client>
        <xrd:service iden:objectType="SERVICE">
            <iden:xRoadInstance>PLAYGROUND</iden:xRoadInstance>
            <iden:memberClass>GOV</iden:memberClass>
            <iden:memberCode>8765432-1</iden:memberCode>
            <iden:subsystemCode>TestService</iden:subsystemCode>
            <iden:serviceCode>listMethods</iden:serviceCode>
        </xrd:service>
        <xrd:userId>tuser</xrd:userId>
        <xrd:id>ID11234</xrd:id>
        <xrd:protocolVersion>4.0</xrd:protocolVersion>
    </SOAP-ENV:Header>
    <SOAP-ENV:Body>
        <xrd:listMethods/>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```

curl

```
$ curl -d @listMethods.xml --header "Content-Type: text/xml" -X POST http://testcomss01.playground.
x-road.global
Warning: Couldn't read data from file "listMethods.xml", this makes an empty 
Warning: POST.
<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Body><SOAP-ENV:Fault><faultcode>Client.InvalidSoap</faultcode><faultstring>org.xml.sax.SAXParseException; Premature end of file.</faultstring><faultactor></faultactor><detail><faultDetail xmlns="">9f1abcb5-1976-4b00-b8d4-d4b0f502bbd8</faultDetail></detail></SOAP-ENV:Fault></SOAP-ENV:Body></SOAP-ENV:Envelope>
```

## SUOMI.FI FINNISH SERVICE CATALOGUE

```
$ curl -X GET -H 'X-Road-Client: PLAYGROUND/COM/1234567-8/TestClient' -i 'http://testcomss01.playgr
ound.x-road.global/r1/PLAYGROUND/GOV/8765432-1/TestService/PTV/Organization' > ListOrganizations.ou 
t
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 81950    0 81950    0     0  81950      0 --:--:--  0:00:01 --:--:-- 45603
```

[ListOrganization.out](./ListOrganization.out)


```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/xroad/playground (master)
$ curl -X GET -H 'X-Road-Client: PLAYGROUND/COM/1234567-8/TestClient' -i 'http://testcomss01.playgr 
ound.x-road.global/r1/PLAYGROUND/GOV/8765432-1/TestService/PTV/Organization/97b5355c-1d74-407b-9508 
-452b376be153' > DetailOrganization.out
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 13018  100 13018    0     0  13018      0  0:00:01  0:00:01 --:--:-- 11409
```

[DetailOrganization.out](./DetailOrganization.out)


```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/xroad/playground (master)
$ curl -X GET -H 'X-Road-Client: PLAYGROUND/COM/1234567-8/TestClient' -i 'http://testcomss01.playgr
ound.x-road.global/r1/PLAYGROUND/GOV/8765432-1/TestService/TR/?totalResults=false&maxResults=10&res 
ultsFrom=0&companyRegistrationFrom=2019-02-28' > ListNotice.out
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  2733  100  2733    0     0   1366      0  0:00:02  0:00:02 --:--:--  1121
```

[ListNotice.out](./ListNotice.out)

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/xroad/playground (master)
$ curl -X GET -H 'X-Road-Client: PLAYGROUND/COM/1234567-8/TestClient' -i 'http://testcomss01.playgr
ound.x-road.global/r1/PLAYGROUND/GOV/8765432-1/TestService/BIS?totalResults=false&maxResults=10&res 
ultsFrom=0&companyRegistrationFrom=2019-02-28' > ListCompanies.out
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1960  100  1960    0     0   1960      0  0:00:01  0:00:01 --:--:--  1003

```

[ListCompanies.out](./ListCompanies.out)

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/xroad/playground (master)
$ curl -X GET -H 'X-Road-Client: PLAYGROUND/COM/1234567-8/TestClient' -i 'http://testcomss01.playgr
ound.x-road.global/r1/PLAYGROUND/GOV/8765432-1/TestService/XRoadStatistics/instances' > ListEnv.out
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   187  100   187    0     0     93      0  0:00:02  0:00:02 --:--:--    72
```

[ListEnv.out](./ListEnv.out)

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/xroad/playground (master)
$ curl -X GET -H 'X-Road-Client: PLAYGROUND/COM/1234567-8/TestClient' -i 'http://testcomss01.playgr
ound.x-road.global/r1/PLAYGROUND/GOV/8765432-1/TestService/XRoadStatistics/instances/EE' > ListStat
istics.out
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   277  100   277    0     0    138      0  0:00:02  0:00:02 --:--:--   108
```

[ListStatistics.out](./ListStatistics.out)

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/egov/xroad/playground (master)
$ curl -X GET -H 'X-Road-Client: PLAYGROUND/COM/1234567-8/TestClient' -i 'http://testcomss01.playgr
ound.x-road.global/r1/PLAYGROUND/GOV/8765432-1/TestService/getOpenAPI?serviceCode=XRoadStatistics'  
> GetApi.out
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  4272  100  4272    0     0   4272      0  0:00:01  0:00:01 --:--:--  2508
```

![GetApi.out](./GetApi.out)

## 進一步資訊

* X-Road Developer Resource
    * https://x-road.global/resources
* X-ROAD ACADEMY -- https://x-road.global/academy
    * X-Road Academy provides online training for developers, users, operators, consultants, service providers and for anyone willing to learn more about the X-Road data exchange layer.
    * [ONLINE TRAINING PART 1: SETTING UP A LOCAL X-ROAD DEVELOPMENT ENVIRONMENT(PDF)](https://www.dropbox.com/s/fp9d2i6kaj103o2/X-Road_Academy-Session_1.pdf) [Video](https://www.dropbox.com/s/851ixifbbq68l50/X-Road_Academy-Session_1.mp4?dl=0)
    * [ONLINE TRAINING PART 2: CONFIGURING CENTRAL SERVER IN A LOCAL X-ROAD DEVELOPMENT ENVIRONMENT (Video)](https://x-road.global/events/2018/12/4/online-training-configuring-a-local-x-road-development-environment)
    * [ONLINE TRAINING PART 3: CONFIGURING CENTRAL SERVER'S SECURITY SERVER AND MANAGEMENT SERVICES IN A LOCAL X-ROAD DEVELOPMENT ENVIRONMENT (Video)](https://www.dropbox.com/s/cmhbs1zdpfj7pib/X-Road_Academy-Session_3.mp4?dl=0)


