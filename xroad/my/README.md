# ccc 的 X-Road 筆記

X-Road 是用來做《服務認證》用的，在 [X-Road Playground](https://x-road.global/xroad-playground) 裏有下列範例：

![](./img/X-Road_Playground.png)

其中其中 Central Services 是政府，Provider 是《服務提供部門》(通常為政府部門)，Consumer 是《服務使用單位》(通常為公司)。

X-Road 裏有 Central Server (CS) 與 Security Server (SS)。


## 我的理解

政府利用 CS 核發服務許可，然後各單位則用 SS 取得許可並提供服務。

在各單位提供服務時，使用者可以取得該單位的公鑰，驗證該單位是否有權提供服務，在安全的憑證環境下，使用該服務。

私鑰可以由使用者自行產生，然後透過簽章向對方(或中央) 證明是對應《公鑰》的擁有者，完成程序後，就可以開始提供服務(或者擁有錢包)。

透過這樣的方式，我們可以經營任何需要《安全認證》的服務，包含像 X-Road 這樣的數位政府，或者比特幣這樣的貨幣系統。

## Central Services

政府利用 Central Server 管理各項服務，以下是 Playground 中的管理畫面。

![](./img/CentralService1.png)

![](./img/CentralService2client.png)


![](./img/CentralService3monitor.png)

![](./img/CentralService3monitorCertificate.png)

![](./img/CentralService3setting.png)

## Provider

![](./img/provider.png)

![](./img/Provider2.png)

![](./img/Provider2certificate.png)

![](./img/Provider2setting.png)

## Consumer

![](./img/Consumer1.png)

![](./img/Consumer2client.png)

![](./img/Consumer2setting.png)
