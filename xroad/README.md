# X-road 

* 資訊入口 -- https://e-estonia.com/solutions/
    * https://e-estonia.com/wp-content/uploads/e-estonia-210820-eng.pdf

X-road 是愛沙尼亞的數位政府核心程式，主要使用 Java 寫成！

* github 專案 -- https://github.com/nordic-institute/X-Road

關於法律的部分請參考 

* [愛沙尼亞數位治理之法律架構](https://eid.hsiaoa.tw/estonian-x-road/)

* 影片 -- [X-road introduction](https://www.youtube.com/watch?v=9PaHinkJlvA)

* https://e-estonia.com/solutions/interoperability-services/x-road/


![X-Road Overview](./img/X-Road_overview.png)

## 程式人員資訊

* X-Road Development -- https://github.com/nordic-institute/X-Road-development
    * [Article: X-Road – a Secure Open Source Data Exchange Layer](https://www.apiscene.io/lifecycle/article-x-road-a-secure-open-source-data-exchange-layer/)
    * NIIS is responsible for developing X-Road core technology, and welcomes everyone to submit source code contributions, new ideas and enhancement requests regarding X-Road. The [backlog](https://jira.niis.org/projects/XRDDEV/) is public — anyone can access it, leave comments and submit enhancement requests through the [X-Road Service Desk portal](https://jira.niis.org/servicedesk/customer/portal/1). Accessing the backlog and service desk requires [creating an account](https://jira.niis.org/secure/Signup!default.jspa) which can be done in few seconds using the [signup form](https://jira.niis.org/secure/Signup!default.jspa).

* https://x-road.global/resources


* https://hub.docker.com/r/niis/xroad-security-server-standalone
    * https://github.com/digitaliceland/Straumurinn/blob/master/DOC/Manuals/standalone_security_server_tutorial.md

```
$ docker pull niis/xroad-security-server-standalone

# Publish the container ports (4000 and 80) to localhost (loopback address).
$ docker run -p 4000:4000 -p 80:80 --name ss niis/xroad-security-server-standalone:bionic-6.23.0
```

* https://confluence.niis.org/display/XRDKB/X-Road+Knowledge+Base
    * [How to Set Up a Security Server?](https://confluence.niis.org/pages/viewpage.action?pageId=4292920)
    * [How to Set Up a Central Server?](https://confluence.niis.org/pages/viewpage.action?pageId=4292922)
    * [How to Configure External Load Balancer for the Security Server?](https://confluence.niis.org/pages/viewpage.action?pageId=4292927)

## Overview

* https://x-road.global/
    * https://x-road.global/x-road-technology-overview
    * https://x-road.global/x-road-organizational-model
    * https://x-road.global/architecture
    * https://x-road.global/development-roadmap
    * https://x-road.global/xroad-edelivery-gateway
    * https://x-road.global/sustainability
    * https://x-road.global/developers
    * https://x-road.global/xroad-history
* http://janno-p.github.io/XRoadProvider/articles/guide/index.html

## 其他國家資源

* https://github.com/digitaliceland/Straumurinn/blob/master/DOC/Manuals/standalone_security_server_tutorial.md

## 參考文獻

* https://en.wikipedia.org/wiki/X-Road

